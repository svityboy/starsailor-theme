# Starsailor

![Starsailor](https://raw.githubusercontent.com/starise/starsailor/master/screenshot.png)

Starsailor is a [Saga](https://wordpress.com/theme/saga) derived theme built for [Starsailor.it](http://starsailor.it) website.

## Requirements

* PHP >= 7.1
* Wordpress >= 5.6
* Saga >= 1.0.3
* Solero >= 1.1.0

## Copyright

Copyright © 2015-2021 [Andrea Brandi](http://andreabrandi.com)
