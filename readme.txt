

== Changelog ==

= 23 January 2020 =
* Fix incorrect Genericons enqueue URI.

= 6 July 2017 =
* Add hover styles to social icons widget.

= 7 June 2017 =
* Update JavaScript that toggles hidden widget area, to make sure new video and audio widgets are displaying correctly when opened.

= 22 March 2017 =
* add Custom Colors annotations directly to the theme
* move fonts annotations directly into the theme

= 9 February 2017 =
* Check for is_wp_error() in cases when using get_the_tag_list() to avoid potential fatal errors.

= 17 June 2016 =
* Add a class of .widgets-hidden to the body tag when the sidebar is active; allows the widgets to be targeted by Direct Manipulation.

= 7 June 2016 =
* Add Headstart annotations;

= 13 April 2016 =
* Ensure we escape $content on output.
* Refactor how to strip first gallery from the content.

= 31 July 2015 =
* Remove .`screen-reader-text:hover` and `.screen-reader-text:active` style rules.

= 15 July 2015 =
* Always use https when loading Google Fonts.

= 1 June 2015 =
* Remove color property of input in widget area because background is white.

= 27 March 2015 =
* Only regex the first gallery for Gallery posts

= 25 March 2015 =
* Update screenshot.png to better represent the theme

= 24 March 2015 =
* Fix the featured image display on single posts/pages
* Ensure videos and images only display an excerpt
* Don't display the_excerpt() unless a post has a featured image assigned
* Style "more-link" more like the original theme
* Attempt to fix position of Skip to content link so it displays below the WP.com toolbar
* Allow long post titles to wrap when viewing single post navigation
* Add more spacing to gallery captions
* Better spacing for comments area/pingbacks/trackbacks
* Ensure comments navigation clears; update styles for screen reader text to match _s
* Ensure labels display inline rather than block
* Fix aria-controls property in header.php to point to the correct ID
* Remove invalid CSS property
* Don't link featured image on pages
* Ensure formatted posts always display a link to the single post view; add screenshot; don't link post title for single link formatted posts
* Tweaks to blockquote styles;

= 23 March 2015 =
* Tweaks to entry meta for status and quote post formats; simplify entry meta function and use only one, rather than two separate functions for index/archives and single posts
* Show line after entry-title on normal posts; only remove it on posts that are not formatted and have featured images
* Adjustments to post meta on index/archives to better reflect the original design
* Style forms to better match the original theme; remove footer post meta on index/archives to avoid overflow for posts with featured images
* Adjustments for site description on mobile devices
* More adjustments to posts with featured images to avoid breaking in most situations
* Don't display featured image for Gallery, Video, or Audio formatted posts
* Adjust styling on index/archives posts with featured images to avoid breaking as much as possible; shorten default excerpt length to help with this as well
* Ensure Related Posts headline font family matches the theme
* Allow featured images on pages;
* Add and style single post navigation

= 20 March 2015 =
* Ensure select text is readable in Firefox
* Remove widgets on 404 page
* Remove charset declaration
* Add tags to style.css
* Simplify gallery display for gallery formatted posts on content.php template
* Simplify gallery post format gallery display
* Minor comment cleanup; don't default to tiled gallery for gallery formatted posts, use whatever the user has chosen. Simplifies the code and leaves the style up to the user.
* ensure long footer credit text does not wrap to two lines

= 18 March 2015 =
* Use unicode characters rather than copying/pasting the characters for opening and closing smart quotes
* Spacing/indentation cleanup; fix for incorrect function_exists() check in inc/template-tags.php; re-style footer to work better with infinite scroll
* Remove unused function to get first image from the_content(); ensure Image formatted posts display the same way, regardless of whether a FEatured Image is assigned
* Refactor menu to use jQuery so it works with widgets that need a .resize event; also allows for smoother animation on open/close. Remove unused JS file.
* Rename widget area for clarity
* Styles for widgets
* Minor stylesheet cleanup; ensure hovered social links in the menu don't display with underlines
* Style page links
* Allow social links to display in the menu; clean up site titles

= 17 March 2015 =
* Set appropriate default custom header text color; remove underline from icon on hover for entry format
* Remove Todo from content templates
* Add support for Gallery post format; add method for putting galleries above the post content
* Cleanup indentation
* Styling for galleries
* Clean up indentation in style.css
* More indentation cleanup
* Style infinite scroll Older Posts button
* Clean up menu area
* Center logo on small screens
* Indentation cleanup in content.php
* Register a special image size for the logo
* Clean up spacing/indenting
* Add a little breathing room between entry content and entry meta
* Reduce code in content templates by moving into extras.php in a new post_class filter function; add correct $themecolors; fix bug where image formatted posts without a featured image would break the layout; always display site title even if a logo is present
* Remove custom comment form styles that were messing up WP.com's comment form.
* Add WP.com specific files and styles; hide .updated class

= 13 March 2015 =
* Moving from /dev to /pub
