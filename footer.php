<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Saga
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<?php saga_paginate_links(); ?>

		<div class="container">
			<div class="site-info">
				<span><?php printf( __( 'Copyright &#169; 2008-%1$s %2$s.', 'saga' ), date_i18n('Y'), 'Starsailor' ); ?></span>
				<span class="sep"> &middot; </span>
				<?php printf( 'Scritto e mantenuto da <a href="%1$s" rel="designer">%2$s</a>.', 'https://andreabrandi.com', 'Andrea Brandi' ); ?>
			</div><!-- .site-info -->
		</div><!-- .container -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
