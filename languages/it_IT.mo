��    @        Y         �  
   �  
   �     �     �  	   �     �     �     �  
   �     �     �            8   1     j     r     w  E   �  W   �          6     F     L  	   Q     [     q     }     �     �  %   �     �     �     �  #   �     �     
          *     3     @  F   V     �     �  \   �     !	     )	     5	     I	  T   R	  !   �	     �	      �	      
      
     .
     N
  #   n
      �
     �
      �
  "   �
           8    V  
   g  
   r     }     �  
   �     �     �     �  
   �     �     �     �       :   )  
   d     o     x  1   �  _   �               1     6     ;     D     a  
   w     �     �  $   �     �  	   �     �     �     �             
   %     0     @  R   U     �     �  \   �     6     >     G     Z  E   c     �     �     �     �     �     �     �     �     �     �  	   �          
                 @      =           #   	   ;                &           3         "                   4              /   6      :   8                2                                  <      7       ,             -       5   *   .   '                      !   %   0           ?      1       $              )   9      (   >   +   
    % Comments %1$s: %2$s &larr; Older Comments ,  1 Comment All %s posts Archives Archives: %s Author: %s Category: %s Comment navigation Comments are closed. Continue reading %s Continue reading %s <span class="meta-nav">&rarr;</span> Day: %s Edit Headings It looks like nothing was found at this location. Maybe try a search? It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help. Lato font: on or offon Leave a comment Links Menu Month: %s Newer Comments &rarr; Newer posts Next Nothing Found Older posts Oops! That page can&rsquo;t be found. Page Page %s Pages: Playfair Display font: on or offon Post navigation Posted in %1$s Posts navigation Previous Primary Menu Proudly powered by %s Ready to publish your first post? <a href="%1$s">Get started here</a>. Search Results for: %s Skip to content Sorry, but nothing matched your search terms. Please try again with some different keywords. Tag: %s Tagged %1$s Theme: %1$s by %2$s Year: %s comments titleOne thought on &ldquo;%2$s&rdquo; %1$s thoughts on &ldquo;%2$s&rdquo; daily archives date formatF j, Y http://wordpress.org/ monthly archives date formatF Y post date%s post format archive titleAsides post format archive titleAudio post format archive titleChats post format archive titleGalleries post format archive titleImages post format archive titleLinks post format archive titleQuotes post format archive titleStatuses post format archive titleVideos yearly archives date formatY PO-Revision-Date: 2019-01-03 08:52:47+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: it
Project-Id-Version: WordPress.com - Themes - Saga
 % commenti %1$s: %2$s &larr; Commenti meno recenti ,  1 commento Tutti %s i posti Archivi Archivi: %s Autore: %s Categoria: %s Navigazione commenti I commenti sono chiusi. Continua a leggere %s Continua a leggere %s <span class="meta-nav">&rarr;</span> Giorno: %s Modifica Intestazioni Sembra che non ci sia nulla. Provare una ricerca? Non riusciamo a trovare quello che cerchi. Forse eseguire una ricerca potrebbe essere di aiuto. acceso Lascia un commento Link Menu Mese: %s Commenti più recenti &rarr; Articoli più recenti Successivo Nessun risultato Articoli meno recenti Oops! Impossibile trovare la pagina. Pagina Pagina %s Pagine:  on Navigazione articolo Inviato su %1$s Navigazione articoli Precedente Menu principale Funziona grazie a %s Sei pronto a pubblicare il tuo primo articolo? <a href="%1$s">Comincia da qui</a>. Risultati della ricerca per: %s Vai al contenuto Spiacente, nulla corrisponde i termini della tua ricerca. Riprova con parole chiave diverse. Tag: %s Tag %1$s Tema: %1$s di %2$s Anno: %s Un pensiero su &ldquo;%2$s&rdquo; %1$s pensieri su &ldquo;%2$s&rdquo; j F Y http://wordpress.org/ F Y %s Digressioni Audio Chat Gallerie Immagini Link Citazioni Stati Video Y 